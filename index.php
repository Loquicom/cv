<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <link rel="stylesheet" type="text/css" href="css/pico.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/cmd.min.css">

    <script src="js/jquery.min.js"></script>
    <script src="//code.iconify.design/1/1.0.6/iconify.min.js"></script>
</head>
<body>

    <?= file_get_contents('src/splash.html') ?>
    <span id="close-fs" role="button" class="hide" onclick="fullscreen()">X</span>

    <div id="header">
        <h1>Hello</h1>
    </div>
    

    <div id="home" class="container">
        <div id="download" class="grid">
            <div><span role="button" data-tooltip="Télécharger les CV en PDF"><span class="iconify" data-icon="mdi-file-pdf-box"></span></span></div>
            <div><span role="button" data-tooltip="Formulaire de contact"><span class="iconify" data-icon="mdi-contact"></span></span></div>
            <div><span role="button" data-tooltip="Télécharger les CV en CMD"><span class="iconify" data-icon="mdi-console"></span></span></div>
        </div>
        <div class="window">
            <div class="window-toolbar">
                <div class="window-toolbar-left">
                    <span class="iconify" data-icon="mdi-console-line"></span>
                </div>
                <div class="window-toolbar-center">
                    Loquicom terminal
                </div>
                <div class="window-toolbar-right">
                    <span class="window-dot" onclick="show()" style="background:#5AC05A;"></span>
                    <span class="window-dot" onclick="fullscreen()" style="background:#FDD800;"></span>
                    <span class="window-dot" onclick="remove()" style="background:#ED594A;"></span>
                </div>
            </div>
            <div id="cmd">
            </div>
            <div id="pico" class="none">
                <iframe src="./src/pico8.html" title="Pico 8 game"></iframe>
            </div>
        </div>
        <div id="konami-helper" class="secret hide"><span class="iconify" data-icon="mdi-reload"></span>&nbsp;+&nbsp;&uarr;&uarr;&darr;&darr;&larr;&rarr;&larr;&rarr;BA</div>
        <div id="hint" class="secret hide">Close to the secret</div>
    </div>
    
    <div>
        Coucou
    </div>
    

    <script src="js/cmd.js"></script>
    <script src="js/konami.js"></script>
    <script src="js/loaded.js"></script>
    <script type="text/javascript">

        function fullscreen(closeBtn = true) {
            const pos = $('#' + windowMode).position();
            if (!$('#' + windowMode).hasClass('fullscreen')) {
                $('#' + windowMode).css('top', Math.trunc(pos.top) + 'px');
                $('#' + windowMode).css('left', Math.trunc(pos.left) + 'px');
                $('#' + windowMode).css('position', 'fixed');
                localStorage.setItem('show-page', false);
            } else {
                $('#' + windowMode).one('transitionend', function() {
                    $('#' + windowMode).css('top', '');
                    $('#' + windowMode).css('left', '');
                    $('#' + windowMode).css('position', '');
                });
                localStorage.setItem('show-page', true);
            }
            
            $('#' + windowMode).toggleClass('fullscreen');
            if (closeBtn) $('#close-fs').toggleClass('hide');
        }

        function show() {
            if ($('#' + windowMode).hasClass('pasla')) {
                $('#' + windowMode).css('height', '');
                setTimeout(() => {
                    $('#hint').addClass('hide');
                }, 74);
            } else {
                $('#' + windowMode).css('height', '0');
                setTimeout(() => {
                    $('#hint').removeClass('hide');
                }, 74);
            }
            $('#' + windowMode).toggleClass('pasla');
        }

        function remove() {
            $('#' + windowMode).css('height', '0');
            setTimeout(() => {
                $('#hint').addClass('hide');
                $('#konami-helper').removeClass('hide');
            }, 74);
            setTimeout(() => {
                $('.window').remove();
            }, 148);
        }
    </script>
</body>
</html>