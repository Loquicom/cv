$(() => {
    const skip = localStorage.getItem('skip-splash');
    if (skip && skip == 'true') {
        unsplash();
    } else {
        if (skip == null) {
            localStorage.setItem('skip-splash', true);
        }

        $('#splash-logo').removeClass('hide');

        const loaderInterval = loader();
        setTimeout(() => {
            $('#splash-loader').removeClass('hide');
        }, 200);
            
        $('#splash-line-1').removeClass('hide').addClass('anim-typewriter');
        $('#splash-line-1').one('animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd', function() {
            $('#splash-line-2').removeClass('hide').addClass('anim-typewriter');
        });
        $('#splash-line-2').one('animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd', function() {
            $('#splash-line-3').removeClass('hide').addClass('anim-typewriter');
        });
        $('#splash-line-3').one('animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd', function() {
            clearInterval(loaderInterval);
            unsplash();
        });
    }
});

const spinner = {
    interval:80,
    frames:[
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[=&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[==&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[===&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[====&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[=====&nbsp;&nbsp;&nbsp;]",
        "[&nbsp;=====&nbsp;&nbsp;]",
        "[&nbsp;&nbsp;=====&nbsp;]",
        "[&nbsp;&nbsp;&nbsp;=====]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;====]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;===]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;==]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;==]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;===]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;====]",
        "[&nbsp;&nbsp;&nbsp;=====]",
        "[&nbsp;&nbsp;=====&nbsp;]",
        "[&nbsp;=====&nbsp;&nbsp;]",
        "[=====&nbsp;&nbsp;&nbsp;]",
        "[====&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[===&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[==&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[=&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]"]
}

function loader() {
    const grid = document.getElementById('splash-loader')
    const spin = document.createElement('div');
    spin.innerText = spinner.frames[0];
    grid.appendChild(spin);
    let i = 0;
    return setInterval(() => {
        requestAnimationFrame(() => {
            spin.innerHTML = spinner.frames[++i % spinner.frames.length];  
        });
    }, spinner.interval);
}

function unsplash() {
    const showPage = localStorage.getItem('show-page');
    if (showPage && showPage == 'true') {
        $('#splash').addClass('none');
    } else {
        if (showPage == null) {
            localStorage.setItem('show-page', false);
        }

        fullscreen(false);
        setTimeout(() => {
            $('#splash').addClass('none');
            $('#close-fs').toggleClass('hide');
        }, 200);
    }
}