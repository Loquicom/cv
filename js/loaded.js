let windowMode = 'cmd';

$(() => {
    const shell = new Cmd({
        selector: '#cmd',
        busy_text: 'Traitement...',
        unknown_cmd: 'Commande non reconnue, tapez "aide" pour avoir la liste des commandes disponibles'
    });

    /* --- Konami Code --- */

    const easterEgg = new Konami(function() {
        windowMode = (windowMode == 'cmd') ? 'pico' : 'cmd';
        $('#cmd').toggleClass('none');
        $('#pico').toggleClass('none');
    });
});